package com.markbro.dzd.sso.client.web.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DzdSsoClientWebDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DzdSsoClientWebDemoApplication.class, args);
    }
}
