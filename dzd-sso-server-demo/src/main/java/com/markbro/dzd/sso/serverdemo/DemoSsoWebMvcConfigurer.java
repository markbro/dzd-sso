package com.markbro.dzd.sso.serverdemo;

import com.markbro.dzd.sso.server.interceptor.SsoWebMvcConfigurer;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DemoSsoWebMvcConfigurer extends SsoWebMvcConfigurer {
}
