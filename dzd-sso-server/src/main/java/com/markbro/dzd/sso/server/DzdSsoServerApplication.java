package com.markbro.dzd.sso.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DzdSsoServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(DzdSsoServerApplication.class, args);
    }
}
